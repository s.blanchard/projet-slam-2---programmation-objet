package Test;

import java.time.LocalDate;
import java.util.Scanner;

import mesclasses.Film;
import mesclasses.Seance;

public class Main {
	static Scanner saisie = new Scanner (System.in); 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    int nbTicketPT;
	    int nbTicketDT;
		LocalDate date1 = LocalDate.of(2020, 02, 26);
		Seance Seance1 = new Seance(new Film("UnOrdinary",128),date1);
		System.out.println("Présentation de la séance");
		System.out.println("\tTitre du film : " + Seance1.getLefilm().getTitre());
		System.out.println("\tDurée du film : " + Seance1.getLefilm().getDuree());
		System.out.println("Date de la séance : " + Seance1.getDate() + " à 21h00");
		System.out.println("il reste " + Seance1.calculNbPlaceRestante() +" place(s)");
		System.out.println("\nCombien de ticket plein tarif souhaitez-vous ? : ");
		nbTicketPT=saisie.nextInt();
		while(Seance1.AssezDePlace(nbTicketPT)==false) {
			System.out.println("il n'y a pas assez de place restante pour votre réservation");
			System.out.println("Combien de ticket plein tarif souhaitez-vous ? : ");
			nbTicketPT=saisie.nextInt();
		}
		Seance1.reserver(nbTicketPT);
		System.out.println("Combien de ticket demi tarif souhaitez-vous ? : ");
		nbTicketDT=saisie.nextInt();
		while(Seance1.AssezDePlace(nbTicketDT)==false) {
			System.out.println("il n'y a pas assez de place restante pour votre réservation ");
			System.out.println("Combien de ticket demi tarif souhaitez-vous ? : ");
			nbTicketDT=saisie.nextInt();
		}
		Seance1.reserver(nbTicketDT);
		System.out.println("Voici votre reçu : ");
		System.out.println("Nombre de place plein tarif : " + nbTicketPT + "==>" + nbTicketPT + "*" + Seance.pleinTarif +"==>"+ nbTicketPT*Seance.pleinTarif+ " euro");
		System.out.println("Nombre de place demi tarif : " + nbTicketDT + "==>" + nbTicketDT + "*" + Seance.pleinTarif +"==>"+ nbTicketDT*Seance.demiTarif+ " euro");
		System.out.println("Total : " + Seance.CalculMontant(nbTicketDT, nbTicketPT)+" euro");
		System.out.println("il reste maintement " + Seance1.calculNbPlaceRestante() +" place(s)");
		
	}

}
