package mesclasses;

public class Film {
		//Propriétés
		private String titre;
		private int duree;
		
		//constructeur
		public Film()
		{
			titre = "La reine des neiges";
			duree = 200;
		}
		
		public Film(String titre, int duree)
		{
			this.titre=titre;
			this.duree=duree;
		}
		//asseseurs
		public String getTitre()
		{
			return titre;
		}
		public int getDuree()
		{
			return duree;
		}
		//modifieurs
		public void setTitre(String titre)
		{
			this.titre=titre;
		}
		public void setDuree(int duree)
		{
			this.duree=duree;
		}
}
