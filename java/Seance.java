package mesclasses;
import java.time.LocalDate;

public class Seance {

	private Film leFilm;
	private LocalDate laDate;
	private int nbPlacesAchetees;
	
	public static int nbPlacesTotales = 80;
	public static double pleinTarif = 8;
	public static double demiTarif = 5;
	
	public Seance()
	{
		leFilm=null;
		laDate=null;
	}
	
	public Seance (Film leFilm, LocalDate laDate)
	{
		this.leFilm=leFilm;
		this.laDate=laDate;
	}		
	
	public Film getLeFilm() 
	{
		return leFilm;
	}
	
	public void setLeFilm(Film leFilm) 
	{
		this.leFilm=leFilm;
	}
	
	public LocalDate getLaDate() 
	{
		return laDate;
	}
	
	public void setLaDate(LocalDate laDate) 
	{
		this.laDate=laDate;
	}
	
	public int getNbPlacesAchetees() 
	{
		return nbPlacesAchetees;
	}
	
	public void setNbPlacesAchetees(int nbPlacesAchetees) 
	{
		this.nbPlacesAchetees=nbPlacesAchetees;
	}
	
	public int CalculNbPlacesRestantes()
	{
		return nbPlacesTotales-nbPlacesAchetees;
	}
	
	public static double CalculMontant (int nbDemiTarif, int nbPleinTarif)
	{
		double total_p = nbPleinTarif*pleinTarif + nbDemiTarif*demiTarif;
		return total_p;
	}
	
	public boolean AssezDePlace(int nb) 
	{
		if((nbPlacesTotales-nbPlacesAchetees) > nb){
			return true;
		}
		else {
		return false;
		}
	}
	public void reserver(int nb) 
	{
		this.nbPlacesAchetees=nbPlacesAchetees + nb;
	}
	
}