package code;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import Class.Batiment;
import Class.Usine;
import Class.Villa;
import code.module;

public class main {
	public static List<Batiment>Construction=new ArrayList<Batiment>();
	static Scanner saisie = new Scanner (System.in);
	
	public static void main(String[] args) {
		initialisation();
		for(Batiment bat : Construction) {
			System.out.println(bat.toString());
			System.out.println("Impot : "+bat.CalcullImpot());
		}
		System.out.println("l'impot moyen est de " + module.impotMoyen(Construction));
		
	}
	static void initialisation() {
		for(int i = 0; i<2; i++) {
			System.out.println("Saisir le nom du propiétaire : ");
			String nomProprietaire = saisie.next();
			System.out.println("Saisir le numero de rue : ");
			int numRue = (int) ferify();
			System.out.println("Saisir le nom de la rue : ");
			String nomRue = saisie.next();
			System.out.println("Saisir le code postal : ");
			int cp= (int) ferify();
			System.out.println("Saisir la ville : ");
			String ville = saisie.next();
			System.out.println("Saisir la superficie du terrain : ");
			double superficie = ferify();
			Construction.add(new Batiment(nomProprietaire,numRue,nomRue,cp,ville,superficie));
			System.out.println("------------------------------\n");
		}
		for(int i = 0; i<2; i++) {
			System.out.println("Saisir le nom du propiétaire : ");
			String nomProprietaire = saisie.next();
			System.out.println("Saisir le numero de rue : ");
			int numRue = (int) ferify();
			System.out.println("Saisir le nom de la rue : ");
			String nomRue = saisie.next();
			System.out.println("Saisir le code postal : ");
			int cp= (int) ferify();
			System.out.println("Saisir la ville : ");
			String ville = saisie.next();
			System.out.println("Saisir la superficie du terrain : ");
			double superficie = ferify();
			
			System.out.println("Saisir le nom de l'entreprise : ");
			String entrerpise = saisie.next();
			System.out.println("Saisir le nombre l'employers : ");
			int nbEmployer = (int) ferify();
			System.out.println("Saisir le nombre de livraison par jour : ");
			int nbLivraison = (int) ferify();
			
			Construction.add(new Usine(nomProprietaire,numRue,nomRue,cp,ville,superficie,entrerpise,nbEmployer,nbLivraison));
			System.out.println("------------------------------\n");
		}
		
		for(int i = 0; i<2; i++) {
			System.out.println("Saisir le nom du propiétaire : ");
			String nomProprietaire = saisie.next();
			System.out.println("Saisir le numero de rue : ");
			int numRue = (int) ferify();
			System.out.println("Saisir le nom de la rue : ");
			String nomRue = saisie.next();
			System.out.println("Saisir le code postal : ");
			int cp= (int) ferify();
			System.out.println("Saisir la ville : ");
			String ville = saisie.next();
			System.out.println("Saisir la superficie du terrain : ");
			double superficie = ferify();
			
			System.out.println("Saisir le nombre de pièces : ");
			int nbPieces = (int) ferify();
			System.out.println("il y a-il un piscine ?");
			boolean piscine = saisie.nextBoolean();
			
			Construction.add(new Villa(nomProprietaire,numRue,nomRue,cp,ville,superficie,nbPieces,piscine));
			System.out.println("------------------------------\n");
		}
	}
	
	static double ferify() {
		boolean ok =false;
		double num=0;
		do {
			try {
				num=saisie.nextDouble();
				ok=true;
			}
			catch(InputMismatchException ime) {
				saisie.nextDouble();
				System.out.println("La valeur entrée ne corespond pas avec un nombre réesayer");
			}
		}while(ok==false);
		return num;
	}
}
