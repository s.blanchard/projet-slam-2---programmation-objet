package Class;

public class Usine extends Batiment{
	private String entreprise;
	private int nbEmploye;
	private int nbLivraisonJour;
	
	
	/**
	 * retourne le nom de l'entrepirse
	 * 
	 * @return - String
	 */
	public String getEntreprise() {
		return this.entreprise;
	}
	
	/**
	 * modifie le nom de l'entrepirse
	 * 
	 * @param entreprise - chaîne de caractères
	 */

	public void setEntreprise(String entreprise)  {
		this.entreprise=entreprise;
	}
	
	/**
	 * retourne le nombre d'employé
	 * 
	 * @return - Entier
	 */
	public int getNbEmployer() {
		return this.nbEmploye;
	}
	
	/**
	 * modifie le nombre d'employé
	 * 
	 * @param nbEmploye - Entier
	 */
	public void setNbEmployer(int nbEmploye)  {
		this.nbEmploye=nbEmploye;
	}
	
	/**
	 * retourne le nombre de livraison par jour
	 * 
	 * @return - Entier
	 */
	public int getNbLivraisonJour() {
		return this.nbLivraisonJour;
	}
	
	/**
	 * modifie le nombre de livraison par jour
	 * 
	 * @param nbLivraisonJour - Entier
	 */
	public void setLivraisonJour(int nbLivraisonJour)  {
		this.nbLivraisonJour=nbLivraisonJour;
	}
	
	public Usine() {
		super();
		this.entreprise="";
		this.nbEmploye=0;
		this.nbLivraisonJour=0;
	}
	
	public Usine(String nomProprietaire, int numRue, String nomRue, int cp, String ville, double superficie, String entreprise, int nbEmploye, int nbLivraisonJour) {
		super(nomProprietaire, numRue, nomRue, cp, ville, superficie);
		this.entreprise = entreprise;
		this.nbEmploye = nbEmploye;
		this.nbLivraisonJour = nbLivraisonJour;
	}
	
	/**
	 * permet de Calculer le flux de vehicule de l'entreprise
	 * 
	 * @return - Double
	 */
	public double fluxVehicule() {
		double flux;
		flux = (this.nbEmploye*0.75)+this.nbLivraisonJour; 
		return flux;
	}
	
	/**
	 * permet de Calculer le montant des impots
	 * 
	 * @return - Double
	 */
	public double CalcullImpot() {
		double impot=5;
		impot=super.CalcullImpot()+(this.fluxVehicule()*0.05);
		return impot;
	}
	/**
	 * réécriture de la méthode toString()
	 * 
	 * @return - String
	 */
	@Override
	public String toString() {
		
		return super.toString()+ 
				"\n\tNom de l'entrepise : " + this.entreprise+
				"\n\ttotalise "+ this.nbEmploye +" enployer(s)" +
				"\n\tavec "+ this.nbLivraisonJour +" livraison par jour";
	}
	
	/**
	 * réécriture de la méthode equals()
	 * 
	 * @return - boolean
	 */
	public boolean equals(Usine obj) {
		if(super.equals(obj) &&
				this.entreprise == obj.entreprise &&
				this.nbEmploye == obj.nbEmploye &&
				this.nbLivraisonJour == obj.nbLivraisonJour) {
		return true;
		}
		else {
			return false;
		}
	}
	
}
