package Class;

public class Villa extends Batiment{
	int nbPieces;
	boolean piscine;
	
	/**
	 * retourne le nombre de pièces
	 * 
	 * @return - Entier
	 */
	public int getNbPieces() {
		return this.nbPieces;
	}
	
	/**
	 * modifie le nombre de pièces
	 * 
	 * @param nbPieces - entier
	 */
	public void setNbPieces(int nbPieces) {
		this.nbPieces=nbPieces;
	}
	
	/**
	 * retourne si il ya une piscine (true si oui) 
	 * 
	 * @return - boolean
	 */
	public boolean isPiscine() {
		return this.piscine;
	}
	
	/**
	 * modifie si il y a une piscine
	 * 
	 * @param piscine - entier
	 */
	public void setPiscine(boolean piscine) {
		this.piscine=piscine;
	}
	public Villa() {
		super();
		this.nbPieces=4;
		this.piscine=false;
	}
	
	public Villa(String nomProprietaire, int numRue, String nomRue, int cp, String ville, double superficie, int nbPieces, boolean piscine) {
		super(nomProprietaire, numRue, nomRue, cp, ville, superficie);
		this.nbPieces=nbPieces;
		this.piscine=piscine;
	}
	
	/**
	 * permet de Calculer le montant des impots
	 * 
	 * @return - Double
	 */
	public double CalcullImpot() {
		double impot=100*this.nbPieces;
		if(piscine) {
			impot+=500;
		}
		
		return impot;
	}
	
	/**
	 * réécriture de la méthode toString()
	 * 
	 * @return - String
	 */
	@Override
	public String toString() {
		String plus;
		if(this.isPiscine()) {
			plus="\n\tEt possède une piscine";
		}
		else {
			plus="\n\tEt ne possède pas de piscine";
		}
		
		return super.toString()+
				"\n\tTotalise " + this.nbPieces + " pièce(s)"+ plus;
	}
	
	/**
	 * réécriture de la méthode equals()
	 * 
	 * @return - boolean
	 */
	public boolean equals(Villa obj) {
		if(super.equals(obj) &&
				this.piscine == obj.piscine &&
				this.nbPieces == obj.nbPieces) {
			return true;
		}
		else {
			return false;
		}
	}
}
