package Class;


/**
 * Définition de la classe Catalogue
 * 
 * @author Stevan Blanchard
 *
 */
public class Batiment {
	//propriétés privées
	private String nomProprietaire;
	private int numRue;
	private String nomRue;
	private int cp;
	private String ville;
	private double superficie;
	
	/**
	 * retourne le nom du prorietaire
	 * 
	 * @return - String
	 */
	public String getNomProprietaire() {
		return this.nomProprietaire;
	}
	
	/**
	 * modifie le nom du propriétaire
	 * 
	 * @param nomProprietaire - chaîne de caractères
	 */
	public void setNomProprietaire(String nomProprietaire) {
		this.nomProprietaire = nomProprietaire;
	}
	
	/**
	 * retourne le numero de rue
	 * 
	 * @return - Entier
	 */
	public int getNumRue() {
		return this.numRue;
	}
	
	/**
	 * modifie le numero de rue
	 * 
	 * @param numRue - Entier
	 */
	public void setNumRue(int numRue) {
		this.numRue = numRue;
	}
	
	/**
	 * retourne le nom de la rue
	 * 
	 * @return - String
	 */
	public String getNomRue() {
		return this.nomProprietaire;
	}
	
	/**
	 * modifie le nom de la rue
	 * 
	 * @param nomRue - Entier
	 */
	public void setNomRue(String nomRue) {
		this.nomRue = nomRue;
	}

	/**
	 * retourne le code postale
	 * 
	 * @return - Entier
	 */
	public int getCP() {
		return this.cp;
	}
	
	/**
	 * modifie le code postal
	 * 
	 * @param cp - Entier
	 */
	public void setCP(int cp) {
		this.cp = cp;
	}
	
	/**
	 * retourne le nom de la ville
	 * 
	 * @return - String
	 */
	public String getVille() {
		return this.ville;
	}
	
	/**
	 * modifie le nom de la ville
	 * 
	 * @param ville - String
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	/**
	 * retourne la superficie
	 * 
	 * @return - Double
	 */
	public double getSuperficie() {
		return this.superficie;
	}
	
	/**
	 * modifie la superficie
	 * 
	 * @param superficie - Double
	 */
	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}
	
	// constructeurs
	public Batiment() {
		this.nomProprietaire = "";
		this.numRue = 1;
		this.nomRue = "rue du cré";
		this.cp = 55555;
		this.ville = "";
		this.superficie = 0.0;
	}
	
	public Batiment(String nomProprietaire, int numRue, String nomRue, int cp, String ville, double superficie) {
		this.nomProprietaire = nomProprietaire;
		this.numRue = numRue;
		this.nomRue = nomRue;
		this.cp = cp;
		this.ville = ville;
		this.superficie = superficie;
	}
	
	/**
	 * permet de Calculer le montant des impots
	 * 
	 * @return - Double
	 */
	public double CalcullImpot() {
		double impot=5;
		impot=impot*superficie;
		return impot;
	}
	
	/**
	 * réécriture de la methode toString()
	 * 
	 * @return - String
	 */
	@Override
	public String toString() {
		return "le propriétaire est : " + this.nomProprietaire +
				"\n\tl'adrrese est : " + this.numRue+ " "+ this.nomRue + " "+ this.ville + " "+ this.cp +
				"\n\tLa superficie est de : "+this.superficie;
	}
	
	/**
	 * réécriture de la methode equals()
	 * 
	 * @return - boolean
	 */
	public boolean equals(Batiment obj) {
		if(this.nomProprietaire == obj.nomProprietaire
				&& this.numRue == obj.numRue
				&& this.nomRue == obj.nomRue
				&& this.cp == obj.cp
				&& this.ville == obj.ville
				&& this.superficie == obj.superficie) {
			return true;
		}
		else {
			return false;
		}
	}
}
