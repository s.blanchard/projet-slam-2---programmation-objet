package classes;

import java.util.ArrayList;
import java.util.List;

/**
 * Définition de la classe Catalogue
 * 
 * @author Géraldine
 *
 */
public class Catalogue {
	// définition des attributs / propriétés
	
	List<OeuvreEnVente> leCatalogue = new ArrayList<OeuvreEnVente>();
	String nomCatalogue;
	
	// get et set
	public List<OeuvreEnVente> getLeCatalogue() {
		return leCatalogue;
	}
	
	public String getNomCatalogue() {
		return nomCatalogue;
	}
	
	// constructeurs
	public Catalogue() {
		this.leCatalogue= new ArrayList<OeuvreEnVente>();
		this.nomCatalogue="vente_du_louvre.catalogue";
	}
	
	public Catalogue(List<OeuvreEnVente> LeCatalogue,String nomCatalogue) {
		this.leCatalogue= LeCatalogue;
		this.nomCatalogue=nomCatalogue;
	}
	
	// méthodes
	
	/**
	 * permet de consulter une oeuvre dans le catalogue
	 * 
	 * @param index : l'index de l'oeuvre dans le catalogue
	 * @return - chaîne de caractères
	 */
	public String consulter(int index) {
		if(leCatalogue.get(index) == null) {
			return "l'oeuvre indexé n'éxiste pas";
		}
		else {
			return "Vous avez demandé à visualiser l'oeuvre indexée : " + index + "\n\til s'agit de l'oeuvre : "
				+ leCatalogue.get(index).getNumero() + " - " + leCatalogue.get(index).getTitre() + " - "
				+ leCatalogue.get(index).getPrixOeuvre();
		}
	}

	/**
	 * permet d'ajouter une oeuvre au catalogue
	 * 
	 * @param uneOeuvre : un objet Oeuvre
	 * @return vrai si l'oeuvre a pu être ajouté / faux si l'oeuvre est déjà dans le
	 *         catalogue
	 */
	public void ajouter(OeuvreEnVente uneOeuvre) {
		// TODO
		boolean exist=false;
		for(Oeuvres c : leCatalogue) {
			if(c.getNumero()==uneOeuvre.getNumero()) {
				System.out.println("L'oeuvre est déjà renseigner dans le catalogue");
				exist=true;
			}
		}
		if(exist==false) {
			leCatalogue.add(uneOeuvre);
		}
		
	}

	/**
	 * permet de retirer une oeuvre du catalogue
	 * 
	 * @param uneOeuvre - un objet Oeuvre
	 * @return faux si l'oeuvre n'existe pas dans le catalogue / vrai sinon
	 */
	public boolean retirer(OeuvreEnVente uneOeuvre) {
		// TODO
		for(OeuvreEnVente c : leCatalogue) {
			if(c.getNumero() == uneOeuvre.getNumero()
					&& c.getTitre()==uneOeuvre.getTitre()) {
				leCatalogue.remove(c);
				return true;
			}
		}
		return false;
	}

	/**
	 * permet de réserver une oeuvre dont le nom est donné en paramètre, il faut que
	 * cette oeuvre existe dans le catalogue et qu'elle ne soit pas déjà réservée
	 * 
	 * @param uneOeuvre - un objet Oeuvre
	 * @return retourne vrai si l'oeuvre a pu être réservée
	 */

	// TODO
	public boolean reserver(OeuvreEnVente uneOeuvre) {
		int index;
		if(leCatalogue.contains(uneOeuvre)){
			index=leCatalogue.indexOf(uneOeuvre);
			leCatalogue.get(index).reserve();
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * permet de réserver une oeuvre qui se trouve dans la List à l'index passé en
	 * paramètre
	 * 
	 * @param index - index où se situe l'oeuvre dans le catalogue
	 * @return retourne vrai si l'oeuvre est bien réservée
	 */

	// TODO
	public boolean reserver(int e) {
		if(leCatalogue.get(e).getEtat()=='R') {
			return false;
		}
		else {
			leCatalogue.get(e).reserve();
			return true;
		}
	}

	/**
	 * retourne la liste des oeuvres libres
	 * 
	 * @return - une List d'objets Oeuvres dont l'état est 'L' (pour Libre)
	 */
	public List<OeuvreEnVente> listeDispo() {
		// TODO
		List<OeuvreEnVente> dispo = new ArrayList<OeuvreEnVente>();
		for(OeuvreEnVente c : leCatalogue ) {
			if(c.getEtat()=='D') {
				dispo.add(c);
			}
		}
		return dispo;
	}
}
