package classes;

/**
 * Définition de la classe Oeuvre En Vente
 * 
 * @author Géraldine
 *
 */
public class OeuvreEnVente extends Oeuvres {
	// propriétés privées
	private char etat;
	private double prixOeuvre;

	// accesseurs
	/**
	 * retourne l'état de l'oeuvre (Libre ou Réservée)
	 * 
	 * @return - caractère
	 */
	public char getEtat() {
		return etat;
	}

	/**
	 * retourne le prix de l'oeuvre
	 * 
	 * @return - un double
	 */
	public double getPrixOeuvre() {
		return prixOeuvre;
	}

	// constructeurs
	// TODO
	public OeuvreEnVente(){
		super();
		etat='D';
		prixOeuvre=0;
	}
	
	public OeuvreEnVente(int numero , String titre, char etat, double prixOeuvre){
		super(numero, titre);
		this.etat=etat;
		this.prixOeuvre=prixOeuvre;
	}

	// méthodes
	public void disponible() {
		etat = 'D';
	}

	public void reserve() {
		etat = 'R';
	}

	// méthode ToString() : affichage complet et détaillé des valeurs des propriétés
	// des oeuvres en vente
	// prévoir un affichage en fonction de l'état de l'oeuvre (D-isponible ou R-éservé)
	// TODO
	@Override
	public String toString() {
		String r;
		if(this.etat=='D') {
			r="l'oeuvre numéro : " + this.getNumero() + ", intitulée :" + this.getTitre() +" est disponible pour le prix de "+ this.getPrixOeuvre()+ "Eur";
		}
		else {
			r="l'oeuvre numéro : " + this.getNumero() + ", intitulée :" + this.getTitre() +" est indisponible pour le moment ";
		}
		
		return r;
	}
}
