package classes;

/**
 * Définition de la classe Oeuvre
 * 
 * @author Géraldine
 *
 */
public class Oeuvres {
	// propriétés privées
	private int numero;
	private String titre;

	/**
	 * retourne le numéro de l'oeuvre
	 * 
	 * @return - entier
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * retourne le titre de l'oeuvre
	 * 
	 * @return - chaîne de caractère
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * Constructeur par défaut
	 */
	public Oeuvres() {
		this.numero = 0;
		this.titre = "";
	}

	/**
	 * Constructeur avec paramètres pour initialisation
	 * 
	 * @param numero - le numéro de l'oeuvre
	 * @param titre  - le titre de l'oeuvre
	 */
	public Oeuvres(int numero, String titre) {
		this.numero = numero;
		this.titre = titre;
	}

	// méthode toString() pour affichage des valeurs des propriétés de l'oeuvre
	// TODO
	@Override
	public String toString() {
		return "l'oeuvre numéro : " + this.numero + ", est intitulé : " + this.titre;
	}

}