package test;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import classes.OeuvreEnVente;
import classes.OeuvrePretee;
import classes.Oeuvres;

public class main2 {
	static List<Oeuvres> liste = new ArrayList<Oeuvres>();
	static Scanner saisie = new Scanner (System.in);
	
	public static void main(String[] args) {
		do {
			int choix;
			System.out.println("Que souhaitez vous faire"
					+ "\n\t1- Ajouter une oeuvre"
					+ "\n\t2- voir la liste de toutes les oeuvres"
					+ "\n\t3- Voir la liste des oeuvres disponible"
					+ "\n\t4- Supprimer une oeuvre"
					+ "\n\t5- Quitter");
			choix = numero();
			
			while(choix > 5 || choix < 1) {
				System.out.println("Erreur veuillez saisir un nombre valide");
				choix = numero();
			}
			switch(choix) {
			case 1:
				System.out.println("quelle type d'oeuvre souahaitez-vous ajoutez"
						+ "\n\t1- Une oeuvre à vendre"
						+ "\n\t2- Une oeuvre à prêter");
				int type = numero();
				while(type < 1 || type > 2)
				{
					System.out.println("erreur de saisie entrer une valeur entre 1 et 2");
					type = numero();
				}
				if(type == 1 ) {
					vendre();	
				}
				else if(type == 2 ) {
					preter();	
				}
				break;
			case 2:
				Oeuvre();
				break;
			case 3:
				OeuvreDispo();
				PropositionReservation();
				break;
			case 4:
				Suprimmer();
				break;
			case 5:
			
				System.exit(0);
				break;
			default:
				System.out.println("une erreur s'est produit");
			}
		} while(true);
	}
	static void vendre() {
		System.out.println("saisir le numero de l'oeuvre");
		int num = numero();
		while(verifNumExist(num)==true)
		{
			System.out.println("une oeuvre possède déjà se numéro veuillez en saisir un autre");
			num=saisie.nextInt();
		}
		System.out.println("saisir le titre de l'oeuvre");
		String titre=saisie.next();
		System.out.println("saisir l'etat de l'oeuvre, si réserver('R') ou disponible('D')");
		char Etat=saisie.next().charAt(0);
		while(Etat != 'D' && Etat != 'R') {
			System.out.println(Etat);
			System.out.println("une erreur de saisie veuillez saisir 'D' ou 'R' ");
			Etat=saisie.next().charAt(0);
		}
		System.out.println("saisir le prix de l'oeuvre");
		double prix=saisie.nextDouble();
		liste.add(new OeuvreEnVente(num,titre,Etat,prix));
	}
	static void preter() {
		System.out.println("saisir le numero de l'oeuvre");
		int num = numero();
		while(verifNumExist(num)==true)
		{
			System.out.println("une oeuvre possède déjà se numéro veuillez en saisir un autre");
			num = numero();
		}
		System.out.println("saisir le titre de l'oeuvre");
		String titre=saisie.next();
		System.out.println("saisir le nom du propriétaire de l'oeuvre");
		String proprio=saisie.next();
		liste.add(new OeuvrePretee(num,titre,proprio));
	}
	
	static void Oeuvre() {
		for(Oeuvres liste : liste) {
			System.out.println(liste.toString());
			
		}
	}
	
	static void OeuvreDispo() {
		for(Oeuvres liste : liste) {
			if(liste instanceof OeuvreEnVente)
			{
				OeuvreEnVente o = (OeuvreEnVente)liste;
				if(o.getEtat()=='D') {
					System.out.println(o.toString());
				}
			}
		}
	}
	
	static boolean verifNumExist(int num) {
		boolean exist =false;
		for(Oeuvres liste : liste) {
			if(liste.getNumero()==num) {
				exist = true;
			}	
		}
		return exist;
	}
	
	static void PropositionReservation() {
		int choix;
		System.out.println("souhaitez-vous reservez une oeuvre ?"
				+ "\n\t-1 Oui"
				+ "\n\t-2 Non");
		choix = numero();
		while(choix < 1 || choix > 2)
		{
			System.out.println("erreur de saisie entrer une valeur entre 1 et 2");
			choix = numero();
		}
		switch(choix) {
		case 1:
			int num;
			OeuvreEnVente o ;
			System.out.println("quelle est le numéro de l'oeuvre choisie ?");
			num = numero();
			while(verifNumExist(num)==false) {
				System.out.println("Erreur aucune corespondance. saisisez un nombre correct");
				num = numero();
			}
			if(liste.get(num-1) instanceof OeuvreEnVente)
			{
				o = (OeuvreEnVente)liste.get(num-1);
				if(o.getEtat()=='D') {
					System.out.println(o.toString());
					o.reserve();
					System.out.println("l'oeuvre a bien été réservé");
				}
				else {
					System.out.println("cette oeuvre n'est pas disponible");
				}
			}
			else {
				System.out.println("cette oeuvres n'est pas disponible a la vente revenez plus tard");
			}
			break;
		case 2:
			break;
		default:
			System.out.println("une erreur s'est produit");
			break;
		}
	}
	static void Suprimmer() {
		int num;
		System.out.println("quelle est le numéro de l'oeuvre a suprimmer ?");
		num = numero();
		while(verifNumExist(num)==false) {
			System.out.println("Erreur aucune corespondance. saisisez un nombre correct");
			num = numero();
		}
		System.out.println(liste.get(num-1).toString());
		liste.remove(num-1);
		System.out.println("L'oeuvre a bien été suprimmer");
	}
	static int numero() {
		boolean ok =false;
		int num=0;
		do {
			try {
				num=saisie.nextInt();
				ok=true;
			}
			catch(InputMismatchException ime) {
				saisie.next();
				System.out.println("il fallais saisir un entier");
			}
		}while(ok==false);
		return num;
	}
	
	
	
}