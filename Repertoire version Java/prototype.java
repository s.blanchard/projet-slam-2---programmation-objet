package test;

import java.util.Scanner;

public class Prototype {
	
	//variable globale
	static String[][] repertoire= new String[10][2];
	static Scanner saisie = new Scanner (System.in); 
	
		static void InitRepertoire() {
			String nom= "";
			String num= "";
			int num_contact = 0;
			char c = 'o';
			while (c=='o' || c=='O')
			{
				System.out.println("saisir le nom du contact : ");
				nom = saisie.next(); 
				System.out.println("saisir le numero du contact : ");
				num = saisie.next();
				repertoire[num_contact][0]=nom;
				repertoire[num_contact][1]=num;
				num_contact++;
				if (num_contact == 10 )
				{
					System.out.println("le repertoire est plein");
					c = 'c';
				}
				
				else
				{
					System.out.println("voulez-vous continuer (o/n) : ");
					c = saisie.next().charAt(0);
					System.out.println();
				}
			}
		}
	
		static int Menu()//bet 1 and 4
		{
			int Saisie = 0;
			System.out.println("Repertoire telephonique \n\t1 : afficher le repertoire\n\t2 : rechercher dans le repertoire\n\t3 : ajouter dans le repertoire\n\t4 : quitter\nvotre choix :");
			Saisie = saisie.nextInt();
			while (Saisie > 4 || Saisie < 1)
			{
				Saisie = saisie.nextInt();
			}
			System.out.println();
			return Saisie;
		}

		static void AfficheRep()
		{
			int count = 0;
			for(int i=0; i<10 ; i++)
			{
				if (repertoire[i][0] != null){
					System.out.println("nom : " + repertoire[i][0]);
					System.out.println("numero :" + repertoire[i][1]);
				}
				else 
				{
					count++;
				}
				if (count == 10)
					System.out.println("le repertoire est vide");
			}
		}

		static void ChercherNum(String nom)
		{
			for(int i = 0; i<10; i++)
			{
				if (repertoire[i][0]!= null && repertoire[i][1]!= null && repertoire[i][0].startsWith(nom) ) 
				{
					System.out.println("nom : " + repertoire[i][0]);
					System.out.println("numero : " + repertoire[i][1]);
					System.out.println();
				}
			}
		}

		static void AjouterCoordonnees()
		{
			String nom= "";
			String num= "";
			int num_contact = 0;
			String c="o";
			for(int i = 0; i<10 ; i++)
			{
				if(repertoire[i][0] != null)
				{
					num_contact++;
				}
				
			}
			System.out.println("nombre de contact : " + num_contact);
			if (num_contact != 10)
			{
				while (c == "o")
				{
					System.out.println( "saisir le nom du contact : ");
					nom = saisie.next();
					System.out.println("saisir le numero du contact : ");
	    			num = saisie.next();
	    			repertoire[num_contact][0]=nom;
	    			repertoire[num_contact][1]=num;
	    			num_contact++;
	    			if (num_contact == 10 )
	    			{	
	    				System.out.println( "le repertoire est plein");
	    				c = "n";
	    			}
	    			else
	    			{
	    				System.out.println( "voulez-vous continuer (o/n) : ");
	    				c = saisie.next();;

	    			}
				}
			}
			else
			{
				System.out.println( "le repertoire est plein. imposible d'en saisir de nouveaux");
			}
		}
	}
