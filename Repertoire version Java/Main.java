package test;

import java.util.Scanner;

public class Main {
	static Scanner saisie = new Scanner (System.in); 
	
	public static void main(String[] args) {
		repertoire();

	}
	
	static int repertoire()
	{
	  int choix ;
	  String nom;
	  Prototype.InitRepertoire();

	  do
	  {
	    choix = Prototype.Menu();

	    switch(choix)
	    {
	      case 1 :
	    	  Prototype.AfficheRep() ;
	    	  System.out.println();
	        break ;

	      case 2 :
	    	  System.out.println(" quel nom recherchez - vous ? ");
	    	  nom = saisie.next();
	    	  System.out.println("voici le ou les numéro : ");
	    	  Prototype.ChercherNum(nom);
	        break ;

	      case 3 :
	    	  Prototype.AjouterCoordonnees();
	        break ;

	      case 4 :
	    	  System.out.println(" \n\tA U   R E V O I R  :");
	    	  
	        break;

	      default :
	    	  System.out.println("Erreur de saisie ");
	    }
	  }
	  while(choix != 4) ;

	  return 0;
	}

}
